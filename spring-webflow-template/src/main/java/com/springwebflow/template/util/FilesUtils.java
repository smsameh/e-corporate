package com.springwebflow.template.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Contains all file related utilities methods
 * 
 * @author Salah Abu Msameh
 */
public class FilesUtils {
	
	/**
	 * Loads the given props file from class path
	 * 
	 * @param queriesConfigFileName
	 */
	public static void loadPropertiesFile(Properties props, String propsFileName) {
		
		try {
			InputStream in = FilesUtils.class.getClassLoader().getResourceAsStream(propsFileName);
			props.load(in);
			
		} catch (IOException e) {
			Log.error(FilesUtils.class, e);
		}
	}
}
