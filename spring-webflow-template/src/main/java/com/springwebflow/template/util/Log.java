package com.springwebflow.template.util;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Central log
 * 
 * @author Salah Abu Msameh
 */
public class Log {
	/**
	 * Log fatal message
	 * @param source
	 * @param message
	 */
	public static void fatal(Object source, String message) {
		fatal(source, message, null);
	}
	
	/**
	 * Log fatal message
	 * @param source
	 * @param message
	 */
	public static void fatal(Object source, String message, Throwable cause) {
		fatal(source.getClass(), message, cause);
	}
	
	/**
	 * Log fatal message
	 * @param source
	 * @param message
	 */
	public static void fatal(Class source, String message, Throwable cause) {
		logMessage(source, message, cause, Level.FATAL);
	}
	
	/**
	 * Log error message
	 * @param source
	 * @param message
	 */
	public static void error(Object source, String message) {
		error(source, message, null);
	}
	
	/**
	 * Log error message
	 * @param source
	 * @param message
	 */
	public static void error(Object source, Throwable cause) {
		error(source.getClass(), null, cause);
	}
	
	/**
	 * Log error message
	 * @param source
	 * @param message
	 */
	public static void error(Object source, String message, Throwable cause) {
		error(source.getClass(), message, null);
	}
	
	/**
	 * Log error message
	 * @param source
	 * @param message
	 */
	public static void error(Class source, String message, Throwable cause) {
		logMessage(source, message, cause, Level.ERROR);
	}
	
	/**
	 * Log warning message
	 * @param source
	 * @param message
	 */
	public static void warn(Object source, String message) {
		warn(source, message, null);
	}
	
	/**
	 * Log warning message
	 * @param source
	 * @param message
	 */
	public static void warn(Object source, String message, Throwable cause) {
		warn(source.getClass(), message, cause);
	}
	
	/**
	 * Log warning message
	 * @param source
	 * @param message
	 */
	public static void warn(Class source, String message, Throwable cause) {
		logMessage(source, message, cause, Level.WARN);
	}
	
	/**
	 * Log debug message
	 * @param source
	 * @param message
	 */
	public static void debug(Object source, String message) {
		debug(source, message, null);
	}
	
	/**
	 * Log debug message
	 * @param source
	 * @param message
	 */
	public static void debug(Object source, String message, Throwable cause) {
		debug(source.getClass(), message, cause);
	}
	
	/**
	 * Log debug message
	 * @param source
	 * @param message
	 */
	public static void debug(Class source, String message, Throwable cause) {
		logMessage(source, message, cause, Level.DEBUG);
	}
	
	/**
	 * Log info message
	 * @param source
	 * @param message
	 */
	public static void info(Object source, String message) {
		info(source, message, null);
	}
	
	/**
	 * Log info message
	 * @param source
	 * @param message
	 */
	public static void info(Object source, String message, Throwable cause) {
		info(source.getClass(), message, cause);
	}
	
	/**
	 * Log info message
	 * @param source
	 * @param message
	 */
	public static void info(Class source, String message, Throwable cause) {
		logMessage(source, message, cause, Level.INFO);
	}
	
	/**
	 * Log the actual message
	 * @param source
	 * @param message
	 */
	private static void logMessage(Object source, String message, Throwable cause, Level level) {
		
		Logger logger = LogManager.getLogger(source.getClass());
		
		if(level == Level.FATAL) {
			
			if(cause != null) {
				logger.fatal(message, cause);
			} else {
				logger.fatal(message);
			}
			
		} else if(level == Level.ERROR) {
			
			if(cause != null) {
				logger.error(message, cause);
			} else {
				logger.error(message);
			}
			
		} else if(level == Level.WARN) {
			
			if(cause != null) {
				logger.warn(message, cause);
			} else {
				logger.warn(message);
			}
			
		} else if(level == Level.DEBUG) {
			
			if(cause != null) {
				logger.debug(message, cause);
			} else {
				logger.debug(message);
			}
			
		} else if(level == Level.INFO) {
			
			if(cause != null) {
				logger.info(message, cause);
			} else {
				logger.info(message);
			}
		}
	}
}
