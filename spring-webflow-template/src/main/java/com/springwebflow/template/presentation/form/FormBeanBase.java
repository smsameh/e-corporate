package com.springwebflow.template.presentation.form;

import java.io.Serializable;

/**
 * Common behaviors among all the system form beans
 * 
 * @author Salah Abu Msameh
 */
public class FormBeanBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
