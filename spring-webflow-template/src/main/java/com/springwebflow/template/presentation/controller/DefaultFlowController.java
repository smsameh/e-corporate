package com.springwebflow.template.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class DefaultFlowController {

	@GetMapping("/defaultFlow/{flowId}")
	public String showViewStatePage(Model model, @RequestAttribute String flowId) {
		return flowId;
	}
}
