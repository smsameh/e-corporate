package com.springwebflow.template.config.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;
import com.springwebflow.template.control.interceptor.ThymeleafLayoutInterceptor;


/**
 * Application beans configurations 
 * 
 * @author Salah Abu Msameh
 */
@Configuration
public class AppConfig {
	
	/**
	 * Register thymeleaf layout interceptor
	 * @return
	 */
	@Bean
	public MappedInterceptor thymeleafLayoutInterceptor() {
		return new MappedInterceptor(null, new ThymeleafLayoutInterceptor());
	}
}
