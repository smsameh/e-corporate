package com.springwebflow.template.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.webflow.execution.FlowExecutionOutcome;
import org.springframework.webflow.mvc.servlet.AbstractFlowHandler;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CustomFlowHandlerAdapter extends AbstractFlowHandler {
	
	@Override
	public String handleExecutionOutcome(FlowExecutionOutcome outcome, HttpServletRequest request,
			HttpServletResponse response) {
		
		String flowId = outcome.getId();
		
		if(StringUtils.isEmpty(flowId) || flowId.equals("initial")) {
			return "/defaultFlow/initial";
			
		} else if(flowId.equals("confirm")) {
			return "/defaultFlow/confirm";
			
		} else if(flowId.equals("final")) {
			return "/defaultFlow/final";
		}
		
		return "/defaultFlow/initial";
	}
}
