package com.springmvc.template.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.springmvc.template.presentation.Pages;
import com.springmvc.template.presentation.form.LoginFormBean;
import com.springmvc.template.service.auth.LoginService;


/**
 * E-corporate login page controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginSrv;
	
	/**
	 * Prepare login form bean
	 * @return
	 */
	@GetMapping("/login")
	public String loginPage(Model model) {
		model.addAttribute("loginFormBean", new LoginFormBean());
		return Pages.LOGIN;
	}
	
	/**
	 * Perform login action
	 * @return
	 */
	@PostMapping("/login")
	public String login(@ModelAttribute LoginFormBean loginFormBean, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			return Pages.LOGIN;
		}
		
		loginSrv.loginUser(loginFormBean.getPassword(), loginFormBean.getPassword());
		
		return Pages.HOME;
	}
}
