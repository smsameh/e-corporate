package com.springmvc.template.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.springmvc.template.presentation.Pages;


/**
 * E-corporate login page controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class HomePageController {
	
	/**
	 * Prepare login form bean
	 * @return
	 */
	@GetMapping("/homw")
	public String loginPage(Model model) {
		return Pages.HOME;
	}
}
