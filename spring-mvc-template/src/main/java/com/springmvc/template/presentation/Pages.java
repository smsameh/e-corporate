package com.springmvc.template.presentation;

/**
 * Static class contains all system defined pages
 * 
 * @author Salah Abu Msameh
 */
public class Pages {
	
	//corporate pages
	public static final String LOGIN = "loginPage";
	public static final String HOME = "homePage";
}
