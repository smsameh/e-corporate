package com.springmvc.template.presentation.controller;

import org.springframework.stereotype.Controller;

/**
 * Defines the common behavior among all the system controllers 
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class BaseController {

	
}
