package com.springmvc.template.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * To register security filter
 * 
 * @author Salah Abu Msameh
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
	//contains nothing
}
