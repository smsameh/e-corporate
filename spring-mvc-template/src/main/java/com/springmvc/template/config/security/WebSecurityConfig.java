package com.springmvc.template.config.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;


/**
 * Web security configurations
 * 
 * @author Salah Abu Msameh
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	@Qualifier("eCorporateDS")
	private DataSource eCorpDS;
	
	@Autowired
    @Qualifier("userDetailsService")
	private UserDetailsService userDetailsSrv;
	
	/**
	 * @param http
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
			//1. grant access to the resource
			.antMatchers("/resources/**").permitAll()
			//2. require authentication for any url
			.anyRequest().authenticated()
			.and().formLogin().loginPage("/admin/login").permitAll()
			.and().logout().permitAll()
			.and().httpBasic();
	}
	
	/**
	 * Configure users authentication
	 * 
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configureUsersAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		
		//init provider
//		auth.jdbcAuthentication()
//			.dataSource(eCorpDS)
//			.usersByUsernameQuery(QueriesConfig.getUserPasswordQuery())
//			.authoritiesByUsernameQuery(QueriesConfig.getUserRolesQuery());
		
		auth.userDetailsService(userDetailsSrv);
	}
}
