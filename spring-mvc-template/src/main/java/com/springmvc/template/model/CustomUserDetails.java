package com.springmvc.template.model;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Custom user details implementation
 * 
 * @author Salah Abu Msameh
 */
public class CustomUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//granted authorities
	private List<GrantedAuthority> authorities;
	
	private String username;
	private String password;
	private boolean locked;
	private boolean enabled;
	
	/**
	 * constructor
	 * 
	 * @param authorities
	 * @param username
	 * @param password
	 * @param locked
	 * @param enabled
	 */
	public CustomUserDetails(String username, String password, boolean locked, boolean enabled, List<GrantedAuthority> authorities) {
		
		this.username = username;
		this.password = password;
		this.locked = locked;
		this.enabled = enabled;
		
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}
	
	@Override
	public String getUsername() {
		return this.username;
	}
	
	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !this.locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
}
