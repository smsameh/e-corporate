package com.springmvc.template.service.auth;

import org.springframework.stereotype.Service;

/**
 * Login Service is responsible for all the login behavior
 * 
 * @author Salah Abu Msameh
 */
@Service
public class LoginService {
	
	/**
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean loginUser(String userName, String password) {
		return true;
	}
}
