package com.springmvc.template.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents user role db entity
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "USER_ROLES")
public class UserRole implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long userId;
	private String role;
	
	@Id
	@Column(name = "USER_ID")
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Id
	@Column(name = "USER_ROLE")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public int hashCode() {
		return this.role.hashCode() + (int)this.userId;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null || !(obj instanceof UserRole)) {
			return false;
		}
		
		UserRole otherRole = (UserRole) obj;
		
		return (this.userId == otherRole.getUserId()) && this.getRole().equals(otherRole.getRole());
	}
}
