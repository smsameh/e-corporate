package com.springmvc.template.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents user password information db entity
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "USER_PASSWORDS")
public class UserPassword {

	private long userId;
	private String password;
	private Date createDate;
	
	@Id
	@Column(name = "USER_ID")
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name = "USER_PASSWORD")
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
