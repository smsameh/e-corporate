package com.springmvc.template.dao;

/**
 * DAO model
 * 
 * @author Salah Abu Msameh
 */
public interface DaoModel {
	
	/**
	 * Finds the db entity for the given id
	 * 
	 * @param id
	 * @return
	 * @throws DaoException
	 */
	public <T> T findById(long id) throws DaoException;
}
