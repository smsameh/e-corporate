package com.soft.ebanking.ws.dto;

/**
 * This class represents a single account transaction details
 * 
 * @author Salah Abu Msameh
 */
public class AccountTransaction {
	
	private String date;
	private String description;
	private String creditAmount;
	private String debitAmount;
	private String balance;
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCreditAmount() {
		return creditAmount;
	}
	
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
	
	public String getDebitAmount() {
		return debitAmount;
	}
	
	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}
	
	public String getBalance() {
		return balance;
	}
	
	public void setBalance(String balance) {
		this.balance = balance;
	}
}
