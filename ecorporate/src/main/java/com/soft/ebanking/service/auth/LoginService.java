package com.soft.ebanking.service.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soft.ebanking.dao.CorporateDao;
import com.soft.ebanking.dao.entity.Corporate;

/**
 * Login Service is responsible for all the login behavior
 * 
 * @author Salah Abu Msameh
 */
@Service
public class LoginService {
	
	@Autowired
	private CorporateDao corpDao;
	
	/**
	 * 
	 * @param username
	 * @param partialAcc
	 * @return
	 */
	public boolean isValidCredentials(String username, String partialAcc) {
		
		Corporate corporate = corpDao.findCorporateByPartialAcc(partialAcc);
		
		if(corporate == null) {
			return false;
		}
		
		return true;
	}

	/**
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean loginUser(String userName, String password) {
		return true;
	}
}
