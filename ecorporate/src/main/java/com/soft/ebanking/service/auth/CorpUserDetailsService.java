package com.soft.ebanking.service.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.soft.ebanking.dao.CorporateDao;
import com.soft.ebanking.dao.entity.User;
import com.soft.ebanking.dao.entity.UserRole;
import com.soft.ebanking.service.auth.model.CorpUserDetails;

/**
 * This implementation supports the corporate user details specs
 * 
 * @author Salah Abu Msameh
 */
@Component
public class CorpUserDetailsService implements UserDetailsService {
	
	@Autowired
	private CorporateDao corpDao;
	
	private Map<String, UserDetails> users = new HashMap<String, UserDetails>();
	
	//load custom user details
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserDetails userDetails = users.get(username);
		
		if(userDetails != null) {
			return userDetails;
		}
		
		//load user
		User user = corpDao.findCorpUserByName(username);
		
		if(user == null) {
			throw new UsernameNotFoundException("No user found for the given username >> ".concat(username));
		}
		
		//build and update cache
		userDetails = buildUserDetails(user);
		users.put(username, userDetails);
		
		return userDetails;
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	private UserDetails buildUserDetails(User user) {
		
		String username = user.getUsername();
		String password = "user";//needs to be encrypted
		boolean locked = user.getStatus() != 1;
		boolean enabled = user.getStatus() == 1;
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(UserRole role : user.getRoles()) {
			authorities.add(new GrantedAuthority() {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public String getAuthority() {
					return role.getRole();
				}
			});
		}
		
		return new CorpUserDetails(username, password, locked, enabled, authorities) ;
	}
}
