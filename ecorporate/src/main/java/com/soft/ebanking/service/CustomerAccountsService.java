package com.soft.ebanking.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.soft.ebanking.ws.dto.AccountDetails;
import com.soft.ebanking.ws.dto.AccountTransaction;

/**
 * This class contains all the customer accounts related methods
 * 
 * @author Salah Abu Msameh
 */
@Service
public class CustomerAccountsService {
	
	/**
	 * 
	 * @return
	 */
	public List<AccountDetails> getAccountsDetails() {
		
		//List<AccountDetails> accounts = new ArrayList<AccountDetails>();
		return populateDummyData();
	}
	
	/**
	 * Retrieve all the transaction details for the given account and period of time
	 * 
	 * @param accountNo
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<AccountTransaction> getDetailedStatement(String accountNo, Date startDate, Date endDate) {
		
		//WebServiceFactory.getCustomerAccountsWebService();
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	private List<AccountDetails> populateDummyData() {
		
		List<AccountDetails> accounts = new ArrayList<AccountDetails>();
		
		for(int i = 0; i < 5; i++) {
			
			AccountDetails acc = new AccountDetails();
			
			acc.setAccountNo("254" + (i + 1) + "8971" + (i + 1) + "6556" + i);
			acc.setAccountType("Current");
			acc.setStatus("Active");
			acc.setBalance(155 * (i + 1) + 256);
			acc.setCurrency("KWD");
			
			accounts.add(acc);
		}
		
		return accounts;
	}
}
