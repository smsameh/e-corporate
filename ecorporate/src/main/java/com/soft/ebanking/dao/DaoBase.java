package com.soft.ebanking.dao;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Concrete implementation for dao behavior
 * 
 * @author Salah Abu Msameh
 */
public class DaoBase implements DaoModel {

	@Autowired
	protected HibernateSessionFactory hsf;
	
	@Override
	public <T> T findById(long id) throws DaoException {
		return null;
	}
}
