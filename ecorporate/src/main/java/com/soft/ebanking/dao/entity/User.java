package com.soft.ebanking.dao.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Represents user information db entity
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "USERS")
public class User {
	
	private long userId;
	private long corporateId;
	private String username;
	private String displayEnglishName;
	private String displayArabicName;
	private String ipan;
	private String civilId;
	private int type;
	private int status;
	private Date createDate;
	private Date lastLoginDate;
	
	private List<UserRole> roles;
	
	@Id
	@Column(name = "USER_ID")
	@GeneratedValue(generator = "userIncrementGenerator")
	@GenericGenerator(name = "userIncrementGenerator", strategy = "increment")
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name = "CORPORATE_ID")
	public long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(long corporateId) {
		this.corporateId = corporateId;
	}
	
	@Column(name = "USERNAME")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "DISPLAY_NAME_EN")
	public String getDisplayEnglishName() {
		return displayEnglishName;
	}

	public void setDisplayEnglishName(String displayEnglishName) {
		this.displayEnglishName = displayEnglishName;
	}
	
	@Column(name = "DISPLAY_NAME_AR")
	public String getDisplayArabicName() {
		return displayArabicName;
	}

	public void setDisplayArabicName(String displayArabicName) {
		this.displayArabicName = displayArabicName;
	}
	
	@Column(name = "IPAN")
	public String getIpan() {
		return ipan;
	}

	public void setIpan(String ipan) {
		this.ipan = ipan;
	}
	
	@Column(name = "CIVIL_ID")
	public String getCivilId() {
		return civilId;
	}

	public void setCivilId(String civilId) {
		this.civilId = civilId;
	}
	
	@Column(name = "USER_TYPE")
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	@Column(name = "STATUS")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	@Column(name = "CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "LAST_LOGIN_DATE")
	public Date getLastLoginDate() {
		return lastLoginDate;
	}
	
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}
}
