package com.soft.ebanking.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Represents corporate information db entity
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "CORPORATES")
public class Corporate {

	private long corporateId;
	private String englishName;
	private String arabicName;
	private String ipan;
	private int type;
	private int status;
	private Date createDate;
	
	@Id
	@Column(name = "CORPORATE_ID")
	@GeneratedValue(generator = "corpIncrementGenerator")
	@GenericGenerator(name = "corpIncrementGenerator", strategy = "increment")	
	public long getCorporateId() {
		return corporateId;
	}
	
	public void setCorporateId(long corporateId) {
		this.corporateId = corporateId;
	}
	
	@Column(name = "CORP_NAME_EN")
	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	@Column(name = "CORP_NAME_AR")
	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}
	
	@Column(name = "IPAN")
	public String getIpan() {
		return ipan;
	}
	
	public void setIpan(String ipan) {
		this.ipan = ipan;
	}
	
	@Column(name = "CORP_TYPE")
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	@Column(name = "STATUS")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name = "CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
