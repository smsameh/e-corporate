package com.soft.ebanking.dao;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.stereotype.Component;

import com.soft.ebanking.util.Log;

/**
 * Session factor wrapper
 * 
 * @author Salah Abu Msameh
 */
@Component
public class HibernateSessionFactory {
	
	private SessionFactory sessionFactory;
	
	/**
	 * Initialize the session factory
	 * @throws Exception
	 */
	@PostConstruct
	private void init() throws Exception {
		
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure()
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			StandardServiceRegistryBuilder.destroy(registry);
			Log.error(this, e.getMessage());
			throw new Exception(e);
		}
	}
	
	/**
	 * Open new session and begin a transaction
	 * 
	 * @return
	 */
	public Session openSessionAndTransaction() {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		return session;
	}
	
	/**
	 * Close current session and commit transaction
	 * 
	 * @param currSession
	 */
	public void close(Session currSession) {
		currSession.getTransaction().commit();
		currSession.close();
	}
}
