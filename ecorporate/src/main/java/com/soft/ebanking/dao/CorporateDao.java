package com.soft.ebanking.dao;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.soft.ebanking.dao.entity.Corporate;
import com.soft.ebanking.dao.entity.User;

/**
 * Corporate DAO
 * 
 * @author Salah Abu Msameh
 */
@Repository
public class CorporateDao extends DaoBase {
	
	/**
	 * 
	 * @param partialAcc
	 * @return
	 */
	public Corporate findCorporateByPartialAcc(String partialAcc) {
		
		Session session = hsf.openSessionAndTransaction();
		
		try {
			return session.createQuery("FROM Corporate WHERE SUBSTRING(ipan, 8, 5) = :pa", Corporate.class)
					.setParameter("pa", partialAcc)
					.getSingleResult();
			
		} catch (NoResultException ex){
			//ignore
		} finally {
			hsf.close(session);
		}
		
		return null;
	}
	
	/**
	 * find user by given username
	 * 
	 * @param username
	 * @return
	 */
	public User findCorpUserByName(String username) {
		
		Session session = hsf.openSessionAndTransaction();
		
		try {
			return session.createQuery("FROM User WHERE username = :username", User.class)
					.setParameter("username", username)
					.getSingleResult();
			
		} catch (NoResultException ex){
			//ignore
		} finally {
			hsf.close(session);
		}
		
		return null;
	}
}
