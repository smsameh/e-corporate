package com.soft.ebanking.dao;

/**
 * Custom dao exception
 * 
 * @author Salah Abu Msameh
 */
public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
