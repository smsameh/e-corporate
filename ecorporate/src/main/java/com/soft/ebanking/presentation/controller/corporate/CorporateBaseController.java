package com.soft.ebanking.presentation.controller.corporate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soft.ebanking.presentation.controller.BaseController;

/**
 * Common behavior among corporate related action controllers
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping(path = "/corporate")
public class CorporateBaseController extends BaseController {

}
