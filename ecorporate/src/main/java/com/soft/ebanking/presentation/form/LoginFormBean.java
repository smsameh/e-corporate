package com.soft.ebanking.presentation.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Salah Abu Msameh
 */
public class LoginFormBean {
	
	@NotNull
	@Size(min = 3, max = 50)
	private String username;
	
	@NotNull
	@Size(min = 5, max = 5)
	private String partialAcc;
	private String password;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPartialAcc() {
		return partialAcc;
	}
	
	public void setPartialAcc(String partialAcc) {
		this.partialAcc = partialAcc;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
