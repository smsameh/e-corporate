package com.soft.ebanking.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Defines the common behavior among all the system controllers 
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class BaseController {
	
//	@GetMapping("/")
//	public String home() {
//		return "redirect:/admin/home";
//	}
}
