package com.soft.ebanking.presentation.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soft.ebanking.presentation.Pages;
import com.soft.ebanking.service.CustomerAccountsService;
import com.soft.ebanking.ws.dto.AccountDetails;

/**
 * Admin landing/home page controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping(path = "/admin")
public class AdminHomePageController {

	@Autowired
	private CustomerAccountsService customerAccSrv;
	
	/**
	 * Initialize all the page needed objects
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(path = "/home")
	public String initLandingPage(Model model) {
		
		//should not be on the admin
		List<AccountDetails> accounts = customerAccSrv.getAccountsDetails();
		model.addAttribute("accounts", accounts);
		
		return Pages.ADMIN_HOME;
	}
}
