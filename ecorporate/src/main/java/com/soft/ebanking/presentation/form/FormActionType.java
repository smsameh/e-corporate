package com.soft.ebanking.presentation.form;

/**
 * Represents the different form actions types
 * @author Salah Abu Msameh
 */
public enum FormActionType {

	ACTION_CREATE,
	ACTION_EDIT,
	ACTION_DELETE
}
