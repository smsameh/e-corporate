package com.soft.ebanking.presentation.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soft.ebanking.presentation.Pages;
import com.soft.ebanking.presentation.form.admin.AdminLoginFB;

/**
 * Admin login controller 
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping(path = "/admin")
public class AdminLoginController {
	
	/**
	 * Prepare the admin login form bean
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/login")
	public String prepareLoginForm(Model model) {
		
		model.addAttribute("AdminLoginFB", new AdminLoginFB());
        return Pages.ADMIN_LOGIN;
	}
	
	/**
	 * 
	 * @param adminLoginFB
	 * @return
	 */
	@PostMapping("/login")
	public String login(@ModelAttribute AdminLoginFB adminLoginFB) {
		return "redirect:/admin/home";
	}
}
