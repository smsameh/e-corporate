package com.soft.ebanking.presentation.form.admin;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import com.soft.ebanking.presentation.form.BaseFormBean;

/**
 * User management form bean
 * 
 * @author Salah Abu Msameh
 */
public class UserManagementFB extends BaseFormBean {
	
//	@NotNull
//	@Max(200)
	private String username;
	
//	@NotNull
//	@Max(500)
	private String displayEnglishName;
	
//	@NotNull
//	@Max(500)
	private String displayArabicName;
	
//	@NotNull
//	@Max(14)
	private String ipan;
	
//	@NotNull
//	@Max(14)
	private String civilId;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getDisplayEnglishName() {
		return displayEnglishName;
	}
	
	public void setDisplayEnglishName(String displayEnglishName) {
		this.displayEnglishName = displayEnglishName;
	}
	
	public String getDisplayArabicName() {
		return displayArabicName;
	}
	
	public void setDisplayArabicName(String displayArabicName) {
		this.displayArabicName = displayArabicName;
	}
	
	public String getIpan() {
		return ipan;
	}

	public void setIpan(String ipan) {
		this.ipan = ipan;
	}
	
	public String getCivilId() {
		return civilId;
	}
	
	public void setCivilId(String civilId) {
		this.civilId = civilId;
	}
}
