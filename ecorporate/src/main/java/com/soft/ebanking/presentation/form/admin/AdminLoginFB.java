package com.soft.ebanking.presentation.form.admin;

/**
 * Admin login form bean
 * 
 * @author Salah Abu Msameh
 */
public class AdminLoginFB {

	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
