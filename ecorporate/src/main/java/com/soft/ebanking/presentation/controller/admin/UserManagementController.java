package com.soft.ebanking.presentation.controller.admin;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.soft.ebanking.presentation.Pages;
import com.soft.ebanking.presentation.controller.BaseController;
import com.soft.ebanking.presentation.form.FormActionType;
import com.soft.ebanking.presentation.form.admin.UserManagementFB;

/**
 * Users management controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping(path = "/admin")
public class UserManagementController extends BaseController {
	
	/**
	 * Initialize initial page with the user management form bean 
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/usersManagment")
	public String init(Model model) {
		
		model.addAttribute("userManagementFB", new UserManagementFB());
		return Pages.AdminPages.USER_MANAGEMENT_INITIAL;
	}
	
	/**
	 * Confirm from data
	 * 
	 * @param userManagementFB
	 * @return
	 */
	@PostMapping("/usersManagment/confirm")
	public String confirm(Model model, @Valid UserManagementFB userManagementFB, BindingResult validationResult) {
		
		if(validationResult.hasErrors()) {
			return Pages.AdminPages.USER_MANAGEMENT_INITIAL;
		}
		
		model.addAttribute("userManagementFB", userManagementFB);
		
		return Pages.AdminPages.USER_MANAGEMENT_CONFIRM;
	}
	
	/**
	 * Create or Edit user
	 * 
	 * @param userManagementFB
	 * @return
	 */
	@PostMapping("/usersManagment/create")
	public String createUser(Model model) {
		
		//process(null, FormActionType.ACTION_CREATE);
		//model.addAttribute("userManagementFB", userManagementFB);
		
		return Pages.AdminPages.USER_MANAGEMENT_FINAL;
	}

	/**
	 * Edit user
	 * 
	 * @param userManagementFB
	 * @return
	 */
	@PostMapping("/usersManagment/edit")
	public String editUser(Model model, @RequestParam("userId") Long userId) {
		
		UserManagementFB fb = new UserManagementFB();
		
//		fb.setUsername("initiator001");
//		fb.setDisplayEnglishName("User Initiator One");
//		fb.setDisplayArabicName("User Arabic Name");
//		fb.setCivilId("CCCCCCCCCC");
//		fb.setIpan("IPAAAAAN");
		
		model.addAttribute("userManagementFB", fb);
		
		return Pages.AdminPages.USER_MANAGEMENT_INITIAL;
	}
}
