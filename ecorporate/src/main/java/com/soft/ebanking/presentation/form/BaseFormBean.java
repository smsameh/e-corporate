package com.soft.ebanking.presentation.form;

import java.io.Serializable;

/**
 * Common behaviors among all the system form beans
 * 
 * @author Salah Abu Msameh
 */
public class BaseFormBean implements FormBean, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
