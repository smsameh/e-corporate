package com.soft.ebanking.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.soft.ebanking.service.auth.LoginService;
import com.soft.ebanking.presentation.Pages;
import com.soft.ebanking.presentation.form.LoginFormBean;

/**
 * E-corporate login page controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class LoginController {
	
	@Autowired
	private LoginService loginSrv;
	
	/**
	 * Prepare login form bean
	 * @return
	 */
	@GetMapping("/login")
	public String loginPage(Model model) {
		model.addAttribute("loginFormBean", new LoginFormBean());
		return Pages.CORPORATE_LOGIN;
	}
	
	/**
	 * Perform login action
	 * @return
	 */
	@PostMapping("/login")
	public String login(@ModelAttribute LoginFormBean loginFormBean, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			return Pages.CORPORATE_LOGIN;
		}
		
		if(!loginSrv.isValidCredentials(loginFormBean.getUsername(), loginFormBean.getPartialAcc())) {
			return Pages.CORPORATE_LOGIN;
		}
		
		return Pages.CORPORATE_HOME;
	}
}
