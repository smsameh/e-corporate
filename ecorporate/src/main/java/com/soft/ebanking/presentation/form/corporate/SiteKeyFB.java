package com.soft.ebanking.presentation.form.corporate;

import com.soft.ebanking.presentation.form.BaseFormBean;

/**
 * 
 * @author Salah Abu Msameh
 */
public class SiteKeyFB extends BaseFormBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String partialAcc;
	private String answer;
	private String password;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPartialAcc() {
		return partialAcc;
	}
	
	public void setPartialAcc(String partialAcc) {
		this.partialAcc = partialAcc;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
