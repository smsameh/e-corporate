package com.soft.ebanking.presentation;

/**
 * Static class contains all system defined pages
 * 
 * @author Salah Abu Msameh
 */
public class Pages {
	
	//corporate pages
	public static final String CORPORATE_LOGIN = "site-key/corporate-login";
	public static final String CORPORATE_HOME = "site-key/corporate-home";
	
	//back-office pages
	public static final String ADMIN_LOGIN = "admin/login/adminLogin";
	public static final String ADMIN_HOME = "admin/adminHomePage";
	
	/**
	 * Admin Pages
	 */
	public static class AdminPages {
		
		//User Management Pages
		public static final String USER_MANAGEMENT_INITIAL = "/admin/usersManagement/initial";
		public static final String USER_MANAGEMENT_CONFIRM = "/admin/usersManagement/confirm";
		public static final String USER_MANAGEMENT_FINAL = "/admin/usersManagement/final";
	}
}
