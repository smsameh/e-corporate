package com.soft.ebanking.config.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * Web security configurations
 * 
 * @author Salah Abu Msameh
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	@Qualifier("eCorporateDS")
	private DataSource eCorpDS;
	
	@Autowired
    @Qualifier("userDetailsService")
	private UserDetailsService userDetailsSrv;
	
	/**
	 * @param http
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//register charset encoding filter before all the security filters
		registerCharsetEncodingFilter(http);
		
		http.authorizeRequests()
			//1. grant access to the resource
			.antMatchers("/resources/**").permitAll()
			.antMatchers("/sitekey/**").permitAll()
			//2. require authentication for any url
			.anyRequest().authenticated()
			.and().formLogin().loginPage("/admin/login").permitAll()
			.and().logout().permitAll()
			.and().httpBasic();
	}

	/**
	 * Configure users authentication
	 * 
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configureUsersAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsSrv);
	}
	
	/**
	 * Register charset encoding filter
	 */
	private void registerCharsetEncodingFilter(HttpSecurity http) {
		
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
        
		filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);
	}
}
