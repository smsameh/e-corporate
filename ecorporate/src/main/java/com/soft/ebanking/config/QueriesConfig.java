package com.soft.ebanking.config;

import java.util.Properties;

import com.soft.ebanking.util.FilesUtils;

/**
 * Represents the configurations related queries</br>
 * <i>This class loads the file <b>queries.properties</b> located under resources</i> 
 * 
 * @author Salah Abu Msameh
 */
public class QueriesConfig {
	
	private static Properties props = new Properties();
	
	//props file name
	private static final String QUERIES_CONFIG_FILE_NAME = "queries.properties";
	
	//queries
	private static final String QUERY_JDBC_USER_PASSWORD = "jdbc_auth_provider_user_password";
	private static final String QUERY_JDBC_USER_ROLES = "jdbc_auth_provider_user_roles";
	
	static {
		FilesUtils.loadPropertiesFile(props, QUERIES_CONFIG_FILE_NAME);
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getUserPasswordQuery() {
		return props.getProperty(QUERY_JDBC_USER_PASSWORD);
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getUserRolesQuery() {
		return props.getProperty(QUERY_JDBC_USER_ROLES);
	}
}
